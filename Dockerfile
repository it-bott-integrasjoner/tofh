FROM python:3-alpine

# Dependencies -- git is needed for setuptools_scm
RUN apk add --update-cache git
# TODO: Alpine images usually does all kinds of cleanup after installing
# packages, probably to clear the cache.

# Set up everything from a source directory
WORKDIR /usr/local/src/tofh

COPY requirements.txt ./
RUN pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

COPY . .
RUN pip install .

# TODO: What is a good working directory?
WORKDIR /

# TODO: Make a docker entry point script
CMD ["python"]
