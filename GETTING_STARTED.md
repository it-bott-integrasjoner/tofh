# Minimal development environment

```bash
python3 -m venv /path/to/env
source /path/to/env/bin/activate
pip install -r /path/to/repo/requirements.txt
export PYTHONPATH=/path/to/repo
```

In addition, you'll need backends for `celery`. The easiest to get started with
is to run `redis` on Docker:

```bash
docker run -p 6379:6379 --name redis_celery_tofh redis
```

And create a basic configuration:

```bash
cp example.config.yml config.yml
```


## Run

- API: `FLASK_APP=tofh.api.create flask run`
- Consumer: `python -m tofh.plugins.mq`
- Workers: `celery -E -A tofh.app.create worker --loglevel=DEBUG`


## Test

```bash
cd /path/to/repo
python setup.py test

# Pass args to pytest
python setup.py test -a "-k test_tasks"
```
