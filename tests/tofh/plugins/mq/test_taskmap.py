import re

import pytest

from tofh.plugins.mq import taskmap


def cmp_pattern(pattern_string, sre_object):
    """ Compare a string pattern with a compiled SRE_Pattern object. """
    # Note: We *could* compare the ``re.compile(pattern_string) ==
    # sre_object``, but that depends on the ``re.compile`` cache, which feels
    # wrong.
    #
    # We could also compare ``pattern_string == sre_object.pattern``, but I'm
    # not sure if ``re.compile`` always keeps the pattern verbatim?  This test
    # assumes that ``re.compile`` does *not* mutate the pattern in any way,
    # *or* mutates the pattern the same way, if given the same input...
    return re.compile(pattern_string).pattern == sre_object.pattern


def cmp_pattern_list(pattern_strings, sre_objects):
    """ Compare a list of string patterns with a list of compiled patterns. """
    return (len(pattern_strings) == len(sre_objects) and
            all(cmp_pattern(p, c)
                for p, c in zip(pattern_strings, sre_objects)))


@pytest.fixture
def patterns():
    """ A list of regular expression patterns. """
    return [r'^foo+$', r'^ba(?:r|z)$']


@pytest.fixture
def hits():
    """ A list of values that will match one or more ``patterns``. """
    return ['foo', 'fooo', 'bar', 'baz']


@pytest.fixture
def misses():
    """ A list of values that will not match any ``patterns``. """
    return ['fo', 'bat']


def test_regex_collection_init(patterns):
    collection = taskmap.RegexCollection(patterns)
    assert isinstance(collection, taskmap.RegexCollection)


@pytest.fixture
def collection(patterns):
    return taskmap.RegexCollection(patterns)


def test_regex_collection_len(collection, patterns):
    if len(patterns) < 1:
        pytest.xfail("bad patterns fixture")
    assert len(collection) == len(patterns)


def test_regex_collection_iter(collection, patterns):
    if len(patterns) < 1:
        pytest.xfail("bad patterns fixture")
    iter_list = list(iter(collection))
    for i, pattern in enumerate(patterns):
        assert cmp_pattern(pattern, iter_list[i])


def test_regex_collection_call_hits(collection, hits):
    if len(hits) < 1:
        pytest.xfail("bad hits fixture")
    for hit in hits:
        assert collection(hit)


def test_regex_collection_call_misses(collection, misses):
    if len(misses) < 1:
        pytest.xfail("bad misses fixture")
    for miss in misses:
        assert not collection(miss)


@pytest.fixture
def attr_patterns(patterns):
    return {
        'attr': patterns,
        'attr2': patterns,
        'attr3': patterns,
    }


@pytest.fixture
def obj_hits(hits, misses):
    """ Objects that should match ``attr_patterns``. """
    if len(hits) < 1 or len(misses) < 1:
        pytest.xfail("bad fixtures")
    miss = misses[0]
    return [
        type('obj_hit', (object, ), {'attr': hit, 'attr2': miss})
        for hit in hits]


@pytest.fixture
def obj_misses(hits, misses):
    """ Objects that should *not* match ``attr_patterns``. """
    if len(hits) < 1 or len(misses) < 1:
        pytest.xfail("bad fixtures")
    hit = hits[0]
    return [
        type('obj_miss', (object, ), {'attr': miss, 'unrelated': hit})
        for miss in misses]


def test_attribute_matcher_init(attr_patterns):
    matcher = taskmap.AttributeMatcher(attr_patterns)
    assert isinstance(matcher, taskmap.AttributeMatcher)


@pytest.fixture
def matcher(attr_patterns):
    return taskmap.AttributeMatcher(attr_patterns)


def test_attribute_matcher_len(matcher, attr_patterns):
    assert len(matcher) == len(attr_patterns)


def test_attribute_matcher_iter(matcher, attr_patterns):
    assert list(sorted(matcher)) == list(sorted(attr_patterns))


def test_attribute_matcher_get(matcher, attr_patterns):
    for key in attr_patterns:
        assert cmp_pattern_list(attr_patterns[key], list(matcher[key]))


def test_attribute_matcher_call_hits(matcher, obj_hits):
    for obj in obj_hits:
        assert matcher(obj)


def test_attribute_matcher_call_misses(matcher, obj_misses):
    for obj in obj_misses:
        assert not matcher(obj)


@pytest.fixture
def obj():
    """ An object with nested attributes:

    obj.a == 'foo'
    obj.b.c == 'bar'
    obj.d.e == 'baz'
    obj.d.f.g == 'bat'
    obj.d.h == 'bax'
    """
    class NestedAttrs:
        def __init__(self, d):
            self._d = d

        def __getattr__(self, attr):
            if attr in self._d:
                v = self._d[attr]
                if isinstance(v, dict):
                    return type(self)(v)
                else:
                    return v
            return super(NestedAttrs, self).__getattr__(attr)

    return NestedAttrs({
        'a': 'foo',
        'b': {'c': 'bar'},
        'd': {'e': 'baz', 'f': {'g': 'bat'}, 'h': 'bax'},
    })


def test_get_value(obj):
    assert obj.a == taskmap.get_value(obj, 'a')
    assert obj.b.c == taskmap.get_value(obj, 'b.c')
    assert obj.d.f.g == taskmap.get_value(obj, 'd.f.g')
