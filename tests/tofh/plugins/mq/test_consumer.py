from tofh.config import AppConfig
from tofh.plugins import mq


def test_get_consumer():
    config = AppConfig(
        {
            "tofh.plugins.mq": {
                "exchanges": [],
                "queues": [],
                "bindings": [],
            },
        }
    )
    consumer = mq.get_consumer(config)
    assert isinstance(consumer, mq.Consumer)


def test_config():
    config = AppConfig(
        {
            "tofh.plugins.mq": {
                "exchanges": [],
                "queues": [],
                "bindings": [],
            },
        }
    )

    c = mq.config.ConsumerConfig(**config.get("tofh.plugins.mq"))
    assert c.exchanges == []
    assert c.queues == []
    assert c.consume_queues == []
    assert set(x.queue for x in c.queues) | set(c.consume_queues) == set()

    config = AppConfig(
        {
            "tofh.plugins.mq": {
                "exchanges": [
                    {"name": "my-exchange"}],
                "queues": [
                    {"name": "my-first-queue"}],
                "bindings": [
                    {"queue": "my-first-queue",
                     "exchange": "my-exchange",
                     "routing_keys": ["my-first-key", "my-second-key"]}],
                "consume_queues": ["my-second-queue"]
            },
        }
    )

    c = mq.config.ConsumerConfig(**config.get("tofh.plugins.mq"))
    assert len(c.exchanges) == 1
    assert c.exchanges[0] == mq.config.Exchange(name="my-exchange")
    assert len(c.queues) == 1
    assert c.queues[0] == mq.config.Queue(name="my-first-queue")
    assert len(c.consume_queues) == 1
    assert c.consume_queues[0] == "my-second-queue"
    assert (set(x.queue for x in c.bindings) | set(c.consume_queues) ==
            set(["my-first-queue", "my-second-queue"]))
