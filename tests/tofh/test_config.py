from tofh.config import (
    AppConfig,
    CeleryConfig,
    DEFAULT_CONFIG_FLASK,
    DEFAULT_CONFIG_LOGGING,
    DEFAULT_CONFIG_SENTRY,
)
from tofh.tasks import TaskCollection


def test_CeleryConfig():
    assert CeleryConfig() == {}
    assert CeleryConfig({"foo": "bar"}).foo == "bar"


def test_AppConfig_defaults():
    c = AppConfig()
    assert c.celery == {}
    assert c.flask == DEFAULT_CONFIG_FLASK
    assert c.logging == DEFAULT_CONFIG_LOGGING
    assert c.sentry == DEFAULT_CONFIG_SENTRY
    assert c.tasks == TaskCollection()
    assert c.extra == {}


def test_AppConfig_extra():
    c = AppConfig({"foo": "bar"})
    assert c.extra == {"foo": "bar"}
    assert c.get("foo") == "bar"
    assert c.get("bar", "baz") == "baz"


def test_AppConfig_todict():
    """
    As the AppConfig format is not formally defined
    it is not future-proof to test the interior of the dict.
    We are satisfied if all the top-level keys are present.
    """
    extra = {"foo": "bar"}
    raw = AppConfig(extra).to_dict()
    # normally we would use isinstance() but we want the real type in this case
    assert type(raw) == dict
    expected_keys = ["celery", "flask", "logging", "sentry", "tasks", *extra.keys()]
    assert len(expected_keys) == len(raw)
    for key in expected_keys:
        assert key in raw
    for key in extra:
        assert raw[key] == extra[key]
