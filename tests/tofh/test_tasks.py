import itertools

import pytest

import tofh.tasks


def _make_add_task(name):
    """ make a task with a real, importable ``call`` implementation. """
    return tofh.tasks.TaskInfo(name=name, call='operators:add')


@pytest.fixture
def task_generator():
    """ an infinite unique task generator. """
    def _generator():
        for i in itertools.count():
            yield _make_add_task('test_task_{}'.format(i))
    return _generator()


@pytest.fixture
def make_tasks(task_generator):
    """ a function that returns a list of n unique tasks. """
    def _make_tasks(num):
        return list(itertools.islice(task_generator, num))
    return _make_tasks


@pytest.fixture
def tasks(make_tasks):
    """ a list of 3 unique tasks. """
    return make_tasks(3)


# TaskInfo

def task_init():
    t = tofh.tasks.TaskInfo('add', 'operators:add')
    assert t.name == 'add'
    assert t.call == 'operators:add'


def task_entry_point():
    from operators import add
    t = tofh.tasks.TaskInfo('add', 'operators:add')
    assert t.entry_point == add


def task_bad_entry_point():
    bad_entry_point = 'illegal-module:foo'
    with pytest.raises(ValueError) as excinfo:
        tofh.tasks.TaskInfo('foo', bad_entry_point)
    msg = str(excinfo.value)
    assert 'Invalid call' in msg
    assert repr(bad_entry_point) in msg


# TaskCollection

def test_list_init():
    l = tofh.tasks.TaskCollection()
    assert l.tasks == {}


def test_list_len(tasks):
    l = tofh.tasks.TaskCollection()
    assert len(l) == 0

    l = tofh.tasks.TaskCollection(tasks)
    assert len(l) == len(tasks)


def test_list_iter(tasks):
    l = tofh.tasks.TaskCollection(tasks)
    it = iter(l)

    for task in tasks:
        assert task.name == next(it)


def test_list_get(tasks):
    l = tofh.tasks.TaskCollection(tasks)

    for name in l:
        assert name == l[name].name


# TaskDescriptor

@pytest.fixture
def task_cls():
    """ A class with a TaskDescriptor ``tasks``. """
    class TaskClass:
        tasks = tofh.tasks.TaskDescriptor()
    return TaskClass


def test_list_descriptor(task_cls, tasks):
    obj = task_cls()
    obj.tasks = tasks

    assert isinstance(obj.tasks, tofh.tasks.TaskCollection)
    assert list(obj.tasks.values()) == tasks
