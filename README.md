# tofh

*tofh* is a task processing and management engine, built on top of `celery`.
It loads third party task implementations from a configuration file, and queues
these tasks using extenal triggers.

See [Design](#design) for a more thorough explenation of how `tofh` works.

## Install

Before installing, you should upgrade setuptools.

```bash
pip install -U setuptools
```


To install the module, run

```bash
pip install .
# OR
python setup.py install
```

Note that *tofh* is designed to run on `python>=3.6`.

## Configuration

See the file `example.config.yml`.

## Run

`TOFH_CONFIG=/path/to/tofh_config.yml python -m tofh.plugins.mq`

## Run tests

```bash
python setup.py test
```


## Build documentation

```
python setup.py build_sphinx
```

This will install *tofh* and all its dependencies in a temporary environment,
and build the documentation in the `build/sphinx/html` folder.

If you already have `sphinx` and `sphinxcontrib-httpdomain`, you can also do:

```bash
cd docs
make html
```


## Try it out with Docker

TODO: Make some docker entry points, and document them here.

```bash
docker build -f Dockerfile -t tofh .
docker run --rm -it tofh
```

# Design

`tofh` is a task processing framework of modular design, that in its most basic
form, consists of:

* a plugin that schedules tasks for processing.
* an engine that executes scheduled tasks.

## The task execution engine

The task execution engine uses [Celery](https://docs.celeryproject.org) to
persist and execute tasks at appropriate points in time.

The following command starts the task execution engine with 16 workers:

    TOFH_CONFIG=/path/to/tofh_config.yml celery worker --app=tofh.app.create --concurrency=16


Please see the `celery` section in [example.config.yaml](example.config.yaml)
for configuration of the backing store.

## The MQ plugin

The MQ plugin connects to a message broker by AMQP 0.9.1, and:

* Declares the appropriate exchanges
* Declares the appropriate queues
* Binds queues to exchanges
* Consumes queues
* Schedules tasks for execution

The following command starts the MQ plugin:

    TOFH_CONFIG=/path/to/tofh_config.yml python -m tofh.plugins.mq

See the `tofh.plugins.mq` and `tasks` sections in
[example.config.yaml](example.config.yaml) for a configuration example.

## The Scheduler plugin

The scheduler plugin is used for handlers that does not receive notifications

Schedule formats that can be used are:
* crontab
* timedelta

The following command starts the Scheduler plugin:

    TOFH_CONFIG=/path/to/tofh_config.yml celery beat --app=tofh.plugins.scheduler.__main__

See the `tasks` sections in
[example.config.yaml](example.config.yaml) for a configuration example.

## Types of tasks

`tofh` supports both stateless and stateful tasks. The latter allows for a
state to be preserved between invocations of a handler (e.g. keep a database or
HTTP connection open), and can have a great effect on runtime.

### Stateful tasks

To declare a task as stateful, the `task_type` attribute can be set to `stateful` for
a specific task in the `tasks` configuration.

In this mode, a context object will be passed to the handler as the first
positional argument. The context object is mutable and persisted between
invocations on a per-worker basis.

See the `stateful_task` function in
[tofh/demo/__init__.py](tofh/demo/__init__.py) for an example of how a stateful
task can be defined.

### Stateless tasks

Tasks can be declared stateless by setting the `task_type` attribute to
`stateless`, or by omitting the `task_type` attribute in the handler
configuration.

A context object is not passed to stateless task as the first positional argument.

See the `echo` function in [tofh/demo/__init__.py](tofh/demo/__init__.py) for
an example.

### Deciding on a task type

When deciding if a task should be stateful, the following points might be worth considering:

* 3 packets are required to set up a TCP connection
* At least 4 transmissions are required to set up TLS
* 1 transmission is required to tear down TLS
* 3 packets are required to tear down a TCP connection
* How many connections does the handler need to fetch and store information

So if the round trip time between two hosts is 10 milliseconds, the total time
used for setup and teardown of one connection is:

    (3 * 4 * 1 * 3) * (10/2) = 180 ms

If the handler executes 10 000 times, and one connection is established for
each invocation:

    (10 000 * 180 / 1 000 ) / 60 = 30 minutes

The 30 minutes spent setting up and tearing down one connection, should rather be
used to transfer data over the connection.

## Prioritizing tasks

To prioritize tasks, Celery must be configured with `task_queue_max_priority`.
You may also set a `task_default_priority`.

For each task that should have a different priority than the default, set its
`priority` to a number >=0 and <=`task_queue_max_priority`. A higher number
means higher priority.

Note that RabbitMQ doesn't support adding arguments (in this case, `x-max-priority`)
to a queue after it has been declared. This also applies to the task queue created
by Celery (`task_default_queue`). You must either delete and recreate it or use
a new queue name. Use shovels to move messages if needed.

For further reading, see
[Celery: RabbitMQ Message Priorities](https://docs.celeryproject.org/en/stable/userguide/routing.html#rabbitmq-message-priorities)
and
[RabbitMQ: Priority Queue Support](https://www.rabbitmq.com/priority.html).

## Configuring handlers

For an example of how a handler can be configured, please see the function
`use_a_config_variable` in [tofh/demo/__init__.py](tofh/demo/__init__.py), and
the corresponding `demo` attribute in
[example.config.yaml](example.config.yaml).

For the function to find the configuration, the task execution engine should be
started like:

    TOFH_CONFIG=/path/to/tofh_config.yml DEMO_CONFIG=/path/to/tofh_config.yml celery worker --app=tofh.app.create --concurrency=16

# Definitions

* A *transform* is a function that is applied to the payload received from
  the scheduling plugin. The application of the transform results in a set of
  arguments.
* A *handler* is a function that performs a specific operation (e.g. the provisioning of a user).
* A *task* is created by the application of a set of arguments to a *handler*.
