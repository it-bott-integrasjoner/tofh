""" auto setup of the tofh.api flask app.

This module is intended for use with python application servers, such as the
``flask`` debug server or ``gunicorn``.

Example:
::

    $ FLASK_APP=tofh.api.create FLASK_DEBUG=1 flask run

"""

import tofh.logging
from tofh.config import get_config
from . import autoload_api


def setup():
    config = get_config()
    tofh.logging.configure(config)
    return autoload_api(config)


app = setup()
