"""
A basic Flask API for interacting with tofh.
"""
# TODO: This is a horrible mess, and just serves as a basic suggestion for how
# an API could be done. Also:
# - All of the celery-integration needs to be moved into another module
# - All input must be validated

import logging

import flask
from celery.task.control import inspect
from werkzeug import exceptions
from werkzeug.local import LocalProxy

import tofh.app
import tofh.config

TOFH_CONFIG_KEY = 'TOFH_CONFIG'

logger = logging.getLogger(__name__)
api = flask.Blueprint('tasks', __name__)

tofh_config = LocalProxy(lambda: flask.current_app.config[TOFH_CONFIG_KEY])
celery_app = LocalProxy(lambda: tofh.app.autoload_app(tofh_config))


def get_request_data():
    """ Get any form data from the current request context. """
    if flask.request.is_json:
        data = flask.request.get_json(silent=False)
    else:
        data = dict(flask.request.form)
    return data


def queue_task(name, args, kwargs, **settings):
    """ Add a Get any form data from the current request context. """
    logger.info("queue_task name=%r", name)
    return celery_app.tasks[name].apply_async(args=args, kwargs=kwargs,
                                              **settings)


@api.before_request
def log_request_rule():
    """ log request rule match. """
    rule = flask.request.url_rule
    logger.debug("Got request for %r", rule)


@api.route('/')
def root():
    """ list api endpoints. """
    def route_repr(r):
        return {
            'path': str(r.rule),
            'endpoint': str(r.endpoint),
            'methods': tuple(map(str, r.methods or ())),
            'arguments': tuple(map(str, r.arguments or ())),
            'defaults': tuple(map(str, r.defaults or ())),
        }
    return flask.jsonify([
        route_repr(r)
        for r in flask.current_app.url_map.iter_rules()])


#
# Queue
#

@api.route('/queue')
def get_queue():
    """ get the current queue. """
    # TODO: this works exceptionally bad
    i = inspect()
    scheduled = i.scheduled()
    active = i.active()
    reserved = i.reserved()
    return flask.jsonify({
        'scheduled': repr(scheduled),
        'active': repr(active),
        'reserved': repr(reserved),
    })


@api.route('/queue', methods=['POST'])
def add_queue():
    """ add task to queue. """
    # TODO: Figure out a better way to parse the request and validate input
    data = get_request_data()

    logger.info("data: %r", data)
    try:
        name = data.pop('name')
    except (TypeError, KeyError, IndexError):
        raise exceptions.BadRequest('missing name')
    logger.debug("add_queue name=%r", name)

    if name not in tofh_config.tasks:
        raise exceptions.BadRequest('invalid name %r' % (name, ))

    args = tuple(data.pop('args', ()))
    kwargs = dict(data.pop('kwargs', {}))

    result = queue_task(name, args, kwargs)
    return flask.jsonify({
        'result': {
            'id': result.id,
            'href': flask.url_for('.get_result', result=result.id),
        },
        'task': {
            'id': name,
            'href': flask.url_for('.get_task', name=name),
        },
        'args': args,
        'kwargs': kwargs,
    })


#
# Tasks
#

@api.route('/tasks')
def get_task_list():
    """ list available tasks. """
    tasks = tofh_config.tasks
    return flask.jsonify([
        tasks[name].to_dict() for name in sorted(tasks)])


@api.route('/tasks/<name>')
def get_task(name):
    """ get info on the task named ``<name>``. """
    tasks = tofh_config.tasks
    if name in tasks:
        return flask.jsonify(tasks[name].to_dict())
    else:
        raise exceptions.NotFound('no task %r' % (name, ))


#
# Results
#

@api.route('/status/<result>')
def get_result(result):
    """ get result for a queued task with id ``<result>``. """
    meta = celery_app.backend.get_task_meta(result)

    # TODO: Can we figure out which task this result comes from?

    if meta.get('status') == 'PENDING':
        raise exceptions.NotFound('no result %r' % (result, ))

    try:
        flask.jsonify(meta.get('result'))
    except TypeError:
        meta['result'] = repr(meta.get('result'))

    try:
        return flask.jsonify(meta)
    except Exception as e:
        return flask.jsonify({
            'error': repr(e),
            'meta': repr(meta),
        })


def autoload_api(config=None):
    config = config or tofh.config.get_config()

    app = flask.Flask(__name__)
    app.config.from_mapping(config.flask)
    # TODO: We need to store the config somewhere better
    app.config[TOFH_CONFIG_KEY] = config
    app.register_blueprint(api)
    return app
