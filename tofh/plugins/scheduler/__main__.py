#!/usr/bin/env python3
import os
import argparse
import logging
import importlib

import tofh.config
import tofh.app

from celery import Celery
from celery.schedules import crontab
from pytimeparse import parse


broker_url = os.getenv('CELERY_BROKER_URL', 'filesystem://')
broker_dir = os.getenv('CELERY_BROKER_FOLDER', './broker')

for f in ['out', 'processed']:
    if not os.path.exists(os.path.join(broker_dir, f)):
        os.makedirs(os.path.join(broker_dir, f))

app = Celery(__name__,
             config_source=dict(task_always_eager=True))
app.conf.update({
    'broker_url': broker_url,
    'broker_transport_options': {
        'data_folder_in': os.path.join(broker_dir, 'out'),
        'data_folder_out': os.path.join(broker_dir, 'out'),
        'data_folder_processed': os.path.join(broker_dir, 'processed')
    },
    'result_persistent': False,
    'task_serializer': 'json',
    'result_serializer': 'json',
    'accept_content': ['json']})
logger = logging.getLogger()


@app.on_after_finalize.connect
def setup_periodic_tasks(sender, **kwargs):
    c = tofh.config.get_config()
    for task in c.tasks.values():
        for trigger in task.triggers:
            if trigger.get('type') == 'tofh.plugins.scheduler':
                mod_name, f_name = task.call.rsplit(':', 1)
                m = importlib.import_module(mod_name)
                f = getattr(m, f_name)
                if task.schedule_type == 'crontab':
                    logger.info('Setting up periodic task with crontab: %s', task.schedule_frequency)
                    minute, hour, day_of_week, day_of_month, month_of_year = task.schedule_frequency.split()
                    sender.add_periodic_task(
                        crontab(minute=minute, hour=hour, day_of_week=day_of_week,
                                day_of_month=day_of_month, month_of_year=month_of_year),
                        app.task(f).s(),
                    )
                elif task.schedule_type == 'timedelta':
                    logger.info('Setting up periodic task with timedelta: %s', task.schedule_frequency)
                    sender.add_periodic_task(parse(task.schedule_frequency), app.task(f).s(), name=f_name)


def main(inargs=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--config')
    args = parser.parse_args(inargs)

    logger.info("Starting %s", parser.prog)
    logger.debug("args: %r", args)

    logging.basicConfig(level=logging.DEBUG,
                        format="%(levelname)s: %(name)s - %(message)s")
    # Hack: Reduce pika logging... This should be configured properly.
    logging.getLogger('pika').setLevel(logging.INFO)


if __name__ == '__main__':
    main()
