"""Callbacks to handle messages."""

import abc
import json
import logging
import urllib.parse

import tofh.app

from tofh.tasks import get_entry_point
from tofh.plugins.mq.event import Event
from tofh.plugins.mq.taskmap import AttributeMatcher


logger = logging.getLogger(__name__)


class Callback(abc.ABC):
    """Abstrack callback interface."""

    @abc.abstractmethod
    def callback(self, channel, method, properties, body):
        pass

    def __call__(self, channel, method, properties, body):
        logger.debug(
            "got message on channel=%r, method=%r, properties=%r," " body=%r",
            channel,
            method,
            properties,
            body,
        )
        logger.debug(
            "method consumer_tag=%r, exchange=%r, routing_key=%r,"
            " delivery_tag=%r, redelivered=%r",
            method.consumer_tag,
            method.exchange,
            method.routing_key,
            method.delivery_tag,
            method.redelivered,
        )
        logger.debug(
            "header delivery_mode=%r, content_type=%r",
            properties.delivery_mode,
            properties.content_type,
        )
        self.callback(channel, method, properties, body)


# Test callback
# TODO: Finish the mapping callback and remove this?


def parse_json_task(body):
    return json.loads(body)


def parse_form_task(body):
    data = urllib.parse.parse_qs(body)
    name = data.pop("name")[0]
    args = tuple(data.pop("args", None) or ())
    return {
        "name": name,
        "args": args,
        "kwargs": dict((k, data[k][0]) for k in data),
    }


class ExampleCallback(Callback):
    def __init__(self, config):
        self.tasks = config.tasks
        self.celery_app = tofh.app.autoload_app(config)

    def queue_task(self, name, args, kwargs):
        return self.celery_app.tasks[name].delay(*args, **kwargs)

    def callback(self, channel, method, properties, body):
        content_type = properties.content_type or "text/plain"
        content_encoding = properties.content_encoding or "ascii"

        body = body.decode(content_encoding)

        # TODO: This is the same as in the API, for now.
        if "json" in content_type:
            data = parse_json_task(body)
        else:
            data = parse_form_task(body)

        logger.debug("data: %r", data)
        self.queue_task(data["name"], data["args"], data["kwargs"])


class ConsumerCallback(Callback):
    """
    The main callback used by the MQ consumer
    """

    def __init__(self, config):
        """
        :type config: tofh.config.AppConfig
        """
        self.config = config
        self.tasks = config.tasks
        self.celery_app = tofh.app.autoload_app(config)

    def callback(self, channel, method, properties, body):
        """
        :param channel: The AMQP session channel
        :type channel: pika.channel.Channel

        :param method: The delivery metadata:
                       (consumer_tag,
                        delivery_tag,
                        exchange,
                        redelivered,
                        routing_key)
        :type method: pika.spec.Basic.Deliver

        :param properties: The properties and headers
        :type properties: pika.spec.BasicProperties

        :param body: The message-body
        :type body: bytes
        """
        event = Event(method, properties, body)
        matches = self.get_matching_calling_data(event)
        if not matches:
            logger.info("Event did not match any MQ selectors: %r", event)
        for task, trigger, args, kw in matches:
            options = {}
            if task.priority is not None:
                options["priority"] = int(task.priority)
            self.enqueue_call(task.name, args, kw, options)
        channel.basic_ack(delivery_tag=method.delivery_tag)

    def enqueue_call(self, name, args, kwargs, options):
        """
        Enqueues the call described in `task`
        with the provided `args` and `kwargs`
        This class' method is designed to use celery

        :param task: The taks to be called
        :type task: tofh.tasks.TaskInfo

        :type args: tuple
        :type kwargs: dict
        :type options: dict
        """
        logger.info(
            "attempting to enqueue task name=%r, args=%r, kwargs=%r, options=%r",
            name,
            args,
            kwargs,
            options
        )
        p = self.celery_app.tasks[name].apply_async(args=args, kwargs=kwargs, **options)
        logger.info(
            "queued task name=%r, args=%r, kwargs=%r, options=%r, uuid=%r",
            name,
            args,
            kwargs,
            options,
            p.id
        )


    def get_matching_calling_data(self, event):
        """
        Returns a list of (task, trigger, args, kwargs) tuples maintaining
        the order specified in the config file

        :param event: The event object
        :type event: tofh.plugins.mq.event.Event

        :return: (task, trigger, args, kwargs) tuple
        :rtype: tuple(tofh.tasks.TaskInfo, dict, tuple, dict)
        """
        call_list = list()
        for task in self.tasks.values():
            for trigger in task.triggers:
                if trigger.get("type") != "tofh.plugins.mq":
                    continue
                # We have an MQ-trigger. Check the selectors for matches!
                matcher = AttributeMatcher(trigger["select"])
                if not matcher(event):
                    continue
                transform = trigger.get("transform")
                if not transform:
                    logger.error(
                        "Misconfigured trigger-transform in task %s detected", task.name
                    )
                    continue
                try:
                    args, kwargs = self.get_args(event, transform)
                    logger.debug("args=%s, kwargs=%s", args, kwargs)
                    call_list.append((task, trigger, args, kwargs))
                except ModuleNotFoundError:
                    logger.error(
                        "Invalid module name in transform %s located in task %s",
                        transform,
                        task.name,
                        exc_info=True,
                    )
                    continue
                except (ImportError, AttributeError):
                    logger.error(
                        "Invalid function or attribute in transform %s "
                        "located in task %s",
                        transform,
                        task.name,
                        exc_info=True,
                    )
                    continue
                except Exception:
                    logger.error("Invalid transform %s", transform, exc_info=True)
                    continue
        return call_list

    def get_args(self, event, transform):
        """
        :param event: The event object
        :type event: tofh.plugins.mq.event.Event

        :param transform: The transform data
        :type transform: str

        :return: (args, kwargs) tuple corresponding to `event` and `transform`
        :rtype: tuple(tuple, dict)
        """
        logger.debug("Fetching args for transform %s", transform)
        transform_func = get_entry_point("transform", transform).resolve()
        return transform_func(event)
