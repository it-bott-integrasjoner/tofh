"""MQ message representation.

This module contains encapsulation of broker messages. This encapsulation is
used when filtering and mapping messages to tasks.
"""


class Event:
    """
    Groups message metadata and message contents into one object.

    The object also includes some convenience properties/aliases.
    """

    DEFAULT_CONTENT_TYPE = "application/json"
    DEFAULT_CONTENT_ENCODING = "utf-8"

    def __init__(self, method, properties, body):
        self.method = method
        self.properties = properties
        self._body = body

    @property
    def exchange(self):
        return self.method.exchange

    @property
    def routing_key(self):
        return self.method.routing_key

    @property
    def content_encoding(self):
        return self.properties.content_encoding or self.DEFAULT_CONTENT_ENCODING

    @property
    def content_type(self):
        return self.properties.content_type or self.DEFAULT_CONTENT_TYPE

    @property
    def body(self):
        return self._body.decode(self.content_encoding)

    @property
    def raw(self):
        return bytearray(self._body)

    def __repr__(self):
        return (
            "<Event" " method={0.method!r}," " properties={0.properties!r}" ">"
        ).format(self)
