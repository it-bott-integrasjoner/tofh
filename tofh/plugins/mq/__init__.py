"""Message Queue consumer plugin."""

from .consumer import (
    Consumer,
    get_consumer,
)
