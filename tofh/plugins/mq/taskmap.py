"""Map message events to tasks.

This module contains integration code to select tasks from a list of tasks in
order to match with something.

triggers:
  - type: framework.plugins.mq
    select:
      routing_key: foo.bar
    transform: business.message:task_transform

"""
import collections
import re


class RegexCollection(collections.abc.Collection):
    """A collection of regular expressions.

    >>> r = RegexCollection(['^fo+$', '^ba.?$'])
    >>> all(r(word) for word in ('foo', 'bar', 'baz', 'bat'))
    True
    >>> r('foooo')
    True
    >>> r('baar')
    False
    """

    def __init__(self, conditions):
        self.conditions = []
        for c in conditions:
            self.conditions.append(re.compile(c))

    def __contains__(self, item):
        # TODO: This check has no real use case
        return item in self.conditions

    def __len__(self):
        return len(self.conditions)

    def __iter__(self):
        return iter(self.conditions)

    def __call__(self, value):
        """Check if ``value`` matches any regex in this collection."""
        return any(regex.match(value) for regex in self)

    def __repr__(self):
        return "{0.__name__}({1})".format(type(self), repr([r.pattern for r in self]))


def get_value(obj, key):
    """Get a dotted attribute from an object.

    :type obj: object
    :type key: str

    >>> Obj = lambda **kw: type('Obj', (object,), kw)
    >>> o = Obj(foo=Obj(bar='baz'))
    >>> o.foo is get_value(o, 'foo')
    True
    >>> o.foo.bar is get_value(o, 'foo.bar')
    True
    """

    def _get_value(item, attrs):
        if len(attrs) == 0:
            return item
        attr = attrs.pop(0)
        item = getattr(item, attr)
        return _get_value(item, attrs)

    try:
        return _get_value(obj, key.split("."))
    except AttributeError as e:
        # The traceback is not important at this point -- we know it's
        # somewhere in the recursive _get_value.  It's more interesting to see
        # the original object and key.
        raise AttributeError(f"get_value({obj!r}, {key!r}) - {e!s}")


class AttributeMatcher(collections.abc.Mapping):
    """Look for a attribute value in an object.

    Example:

    >>> s = AttributeMatcher({
    ...     'foo.bar': ['foo', 'ba.'],
    ...     'hello': ['[Hh]ello\s+[Ww]orld.?'],
    ... })

    ``s(o)`` will attempt to match ``o.foo.bar`` with the ``s['foo.bar']``
    regular expressions, and ``o.hello`` with the ``s['hello']`` regular
    expression, and return ``True`` if any of the expressions match.
    """

    def __init__(self, selectors):
        """
        :param selectors: The selector mapping
        :type selectors: collections.abc.Mapping
        """
        self.selectors = collections.OrderedDict()
        for key, regex_list in selectors.items():
            self.selectors[key] = RegexCollection(regex_list)

    def __repr__(self):
        return "{0.__name__}({1})".format(type(self), repr(self.selectors))

    def __len__(self):
        return len(self.selectors)

    def __iter__(self):
        return iter(self.selectors)

    def __getitem__(self, value):
        return self.selectors[value]

    def __call__(self, event):
        for key, selector in self.items():
            try:
                value = get_value(event, key)
            except AttributeError:
                # event does not have any attribute of 'key'
                continue
            if selector(value):
                return True
        return False
