""" basic consumer settings. """
import ssl
import os
from typing import Dict, List, Any, Optional

import pika
from pydantic import BaseModel, FilePath, conint


_notset = object()


class Exchange(BaseModel):
    name: str
    exchange_type: str = "topic"
    durable: bool = True
    arguments: Optional[Dict[str, Any]] = None


class Queue(BaseModel):
    name: str
    durable: bool = True
    exclusive: bool = False
    auto_delete: bool = False
    arguments: Optional[Dict[str, Any]] = None


class Binding(BaseModel):
    queue: str
    exchange: str
    routing_keys: List[str]
    arguments: Optional[Dict[str, Any]] = None


class ConsumerConfig(BaseModel):
    """mq listener config."""

    username: str = "guest"
    password: str = "guest"
    host: str = "localhost"
    port: conint(gt=0, lt=0xFFFF) = 5672
    ssl_enable: bool = False
    ca_file: Optional[FilePath] = None
    client_cert_file: Optional[FilePath] = None
    client_key_file: Optional[FilePath] = None

    vhost: str = "/"
    consumer_tag: Optional[str] = None
    exchanges: List[Exchange]
    queues: List[Queue]
    bindings: List[Binding]
    consume_queues: Optional[List[str]] = []


def get_credentials(config):
    """ Get credentials from object. """
    if config.username or config.password:
        return pika.PlainCredentials(config.username, config.password)
    return None


def get_connection_params(config, credentials=_notset):
    """ Get pika connection from config. """
    if credentials is _notset:
        credentials = get_credentials(config)

    ssl_options = None
    if config.ssl_enable:
        ssl_context = ssl.create_default_context(cafile=config.ca_file)
        # load_cert_chain requires first param (certfile) to be a valid file
        # system path. keyfile is optional tough, so we do not need to check if
        # it is defined:
        if config.client_cert_file:
            ssl_context.load_cert_chain(config.client_cert_file, config.client_key_file)

        ssl_options = pika.SSLOptions(ssl_context, config.host)

    return pika.ConnectionParameters(
        config.host,
        config.port,
        virtual_host=config.vhost,
        ssl_options=ssl_options,
        credentials=credentials,
    )
