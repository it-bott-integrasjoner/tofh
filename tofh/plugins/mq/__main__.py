#!/usr/bin/env python3

import argparse
import getpass
import logging
import sys

from collections import defaultdict

import tofh.config

from .consumer import get_consumer


logger = logging.getLogger()


def main(inargs=None):
    # TODO: Configure logging
    logging.basicConfig(
        level=logging.DEBUG, format="%(levelname)s: %(name)s - %(message)s"
    )
    # Hack: Reduce pika logging... This should be configured properly.
    logging.getLogger("pika").setLevel(logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config")
    args = parser.parse_args(inargs)

    logger.info("Starting %s", parser.prog)
    logger.debug("args: %r", args)

    # TODO: Load from file using tofh.config
    config = tofh.config.get_config(args.config)
    if sys.stdin.isatty():
        raw_config = defaultdict(dict, config.to_dict())
        mq_config = defaultdict(dict, raw_config["tofh.plugins.mq"])
        try:
            if not mq_config["username"]:
                mq_config["username"] = input("MQ username: ")
            if not mq_config["password"]:
                mq_config["password"] = getpass.getpass(f"MQ password for {mq_config.get('username', 'guest')}: ")
        except KeyboardInterrupt:
            return
        raw_config["tofh.plugins.mq"].update(mq_config)
        config = tofh.config.AppConfig(raw_config)

    consumer = get_consumer(config)
    try:
        consumer.run()
    except KeyboardInterrupt:
        consumer.stop()


if __name__ == "__main__":
    main()
