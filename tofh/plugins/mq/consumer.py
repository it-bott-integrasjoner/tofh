import enum
import logging
import uuid

import pika

from .callback import ConsumerCallback
from .config import (
    ConsumerConfig,
    get_connection_params,
)


logger = logging.getLogger(__name__)


class Consumer:
    """This is an consumer that will handle unexpected interactions
    with RabbitMQ such as channel and connection closures.

    If RabbitMQ closes the connection, this class will stop and indicate
    that reconnection is necessary. You should look at the output, as
    there are limited reasons why the connection may be closed, which
    usually are tied to permission related issues or socket timeouts.

    If the channel is closed, it will indicate a problem with one of the
    commands that were issued and that should surface in the output as well.

    """

    class STEPS(enum.IntEnum):
        """The setup steps to perform."""

        DECLARE_EXCHANGE = 3
        DECLARE_QUEUE = 2
        BIND_QUEUE = 1
        CONSUME = 0

        @classmethod
        def apply_with_next_step(cls, step, f):
            try:
                next_step = cls(step - 1)
            except ValueError:
                return
            else:
                return f(next_step)

    def __init__(
        self,
        connection_params,
        consume,
        exchanges,
        queues,
        bindings,
        consume_queues,
        setup=STEPS.DECLARE_EXCHANGE,
        consumer_tag=None,
    ):
        self.connection_params = connection_params

        self.init_step = setup
        self.consume = consume
        self.exchanges = exchanges
        self.queues = queues
        self.consume_queues = consume_queues
        self.bindings = bindings
        self.consumer_tag = consumer_tag

        # Connection state variables
        self.should_reconnect = False
        self.was_consuming = False

        self._connection = None
        self._channel = None
        self._closing = False
        self._consumer_tag = None
        self._consuming = False

    def setup(self, channel, step):
        """Configure a channel and start consuming."""

        def on_ok(result):
            logger.debug("setup(%s) ok: %r", step, result.method)

        logger.debug("setup(%s)", step)

        if step == self.STEPS.DECLARE_EXCHANGE:
            for e in self.exchanges:
                channel.exchange_declare(
                    exchange=e.name,
                    exchange_type=e.exchange_type,
                    durable=e.durable,
                    arguments=e.arguments,
                    callback=on_ok,
                )
        elif step == self.STEPS.DECLARE_QUEUE:
            for q in self.queues:
                channel.queue_declare(
                    queue=q.name,
                    durable=q.durable,
                    exclusive=q.exclusive,
                    auto_delete=q.auto_delete,
                    arguments=q.arguments,
                    callback=on_ok,
                )
        elif step == self.STEPS.BIND_QUEUE:
            for b in self.bindings:
                for rk in b.routing_keys:
                    channel.queue_bind(
                        queue=b.queue,
                        exchange=b.exchange,
                        routing_key=rk,
                        arguments=b.arguments,
                        callback=on_ok,
                    )
        elif step == self.STEPS.CONSUME:
            logger.info("consuming from queues=%r on channel=%r", self.queues, channel)
            for queue in set(x.queue for x in self.bindings) | set(self.consume_queues):
                if self.consumer_tag is not None:
                    consumer_tag = "{}-{}".format(self.consumer_tag, str(uuid.uuid4()))
                else:
                    consumer_tag = None
                return_tag = channel.basic_consume(
                    queue=queue,
                    on_message_callback=self.on_message,
                    auto_ack=False,
                    consumer_tag=consumer_tag,
                )
                self.was_consuming = True
                self._consuming = True

                logger.info("consuming %r with consumer tag: %r", queue, return_tag)
        else:
            raise Exception(f"Unknown step, {step!r}")

        # Progress to next setup step
        self.STEPS.apply_with_next_step(step, lambda x: self.setup(channel, x))

    def connect(self):
        """This method connects to RabbitMQ, returning the connection handle.
        When the connection is established, the on_connection_open method
        will be invoked by pika.

        :rtype: pika.SelectConnection

        """
        logger.info("Connecting to %s", self.connection_params)
        return pika.SelectConnection(
            parameters=self.connection_params,
            on_open_callback=self.on_connection_open,
            on_open_error_callback=self.on_connection_open_error,
            on_close_callback=self.on_connection_closed,
        )

    def close_connection(self):
        self._consuming = False
        if self._connection.is_closing or self._connection.is_closed:
            logger.info("Connection is closing or already closed")
        else:
            logger.info("Closing connection")
            self._connection.close()

    def on_connection_open(self, _unused_connection):
        """This method is called by pika once the connection to RabbitMQ has
        been established. It passes the handle to the connection object in
        case we need it, but in this case, we'll just mark it unused.

        :param pika.SelectConnection _unused_connection: The connection

        """
        logger.info("Connection opened")
        self.open_channel()

    def on_connection_open_error(self, _unused_connection, err):
        """This method is called by pika if the connection to RabbitMQ
        can't be established.

        :param pika.SelectConnection _unused_connection: The connection
        :param Exception err: The error

        """
        logger.error("Connection open failed: %s", err)
        self.reconnect()

    def on_connection_closed(self, _unused_connection, reason):
        """This method is invoked by pika when the connection to RabbitMQ is
        closed unexpectedly. Since it is unexpected, we will reconnect to
        RabbitMQ if it disconnects.

        :param pika.connection.Connection _unused_connection: The closed connection obj
        :param Exception reason: exception representing reason for loss of
            connection.

        """
        self._channel = None
        if self._closing:
            self._connection.ioloop.stop()
        else:
            logger.warning("Connection closed, reconnect necessary: %s", reason)
            self.reconnect()

    def reconnect(self):
        """Will be invoked if the connection can't be opened or is
        closed. Indicates that a reconnect is necessary then stops the
        ioloop.

        """
        self.should_reconnect = True
        self.stop()

    def open_channel(self):
        """Open a new channel with RabbitMQ by issuing the Channel.Open RPC
        command. When RabbitMQ responds that the channel is open, the
        on_channel_open callback will be invoked by pika.

        """
        logger.info("Creating a new channel")
        self._connection.channel(on_open_callback=self.on_channel_open)

    def on_channel_open(self, channel):
        """This method is invoked by pika when the channel has been opened.
        The channel object is passed in so we can make use of it.

        Since the channel is now open, we'll declare the exchange to use.

        :param pika.channel.Channel channel: The channel object

        """
        logger.info("Channel opened")
        self._channel = channel
        self.add_on_channel_close_callback()

        self.add_on_cancel_callback()

        # This actually sets up consumption
        self.setup(channel, self.init_step)

    def add_on_channel_close_callback(self):
        """This method tells pika to call the on_channel_closed method if
        RabbitMQ unexpectedly closes the channel.

        """
        logger.info("Adding channel close callback")
        self._channel.add_on_close_callback(self.on_channel_closed)

    def on_channel_closed(self, channel, reason):
        """Invoked by pika when RabbitMQ unexpectedly closes the channel.
        Channels are usually closed if you attempt to do something that
        violates the protocol, such as re-declare an exchange or queue with
        different parameters. In this case, we'll close the connection
        to shutdown the object.

        :param pika.channel.Channel channel: The closed channel
        :param Exception reason: why the channel was closed

        """
        logger.warning("Channel %i was closed: %s", channel, reason)
        self.close_connection()

    def add_on_cancel_callback(self):
        """Add a callback that will be invoked if RabbitMQ cancels the consumer
        for some reason. If RabbitMQ does cancel the consumer,
        on_consumer_cancelled will be invoked by pika.

        """
        logger.info("Adding consumer cancellation callback")
        self._channel.add_on_cancel_callback(self.on_consumer_cancelled)

    def on_consumer_cancelled(self, method_frame):
        """Invoked by pika when RabbitMQ sends a Basic.Cancel for a consumer
        receiving messages.

        :param pika.frame.Method method_frame: The Basic.Cancel frame

        """
        logger.info("Consumer was cancelled remotely, shutting down: %r", method_frame)
        if self._channel:
            self._channel.close()

    def on_message(self, channel, basic_deliver, properties, body):
        """Invoked by pika when a message is delivered from RabbitMQ. The
        channel is passed for your convenience. The basic_deliver object that
        is passed in carries the exchange, routing key, delivery tag and
        a redelivered flag for the message. The properties passed in is an
        instance of BasicProperties with the message properties and the body
        is the message that was sent.

        :param pika.channel.Channel channel: The channel object
        :param pika.Spec.Basic.Deliver basic_deliver: method
        :param pika.Spec.BasicProperties properties:
        :param bytes body: The message body

        """
        logger.info(
            "Received message # %s from %s: %s",
            basic_deliver.delivery_tag,
            properties.app_id,
            body,
        )
        try:
            # Notifications are acked by the ConsumerCallback method, which is
            # called here:
            self.consume(channel, basic_deliver, properties, body)
        except Exception as e:
            logger.exception("Message #%s failed", basic_deliver.delivery_tag)

    def stop_consuming(self):
        """Tell RabbitMQ that you would like to stop consuming by sending the
        Basic.Cancel RPC command.

        """
        if self._channel:
            for tag in self._channel.consumer_tags:
                logger.info("Sending Basic.Cancel for consumer tag %s", tag)
                # The Basic.Cancel commands really should be waited for, but
                # we'll skip that implementation for this case.
                self._channel.basic_cancel(tag)
            self._consuming = False
            self.close_channel()

    def close_channel(self):
        """Call to close the channel with RabbitMQ cleanly by issuing the
        Channel.Close RPC command.

        """
        logger.info("Closing the channel")
        self._channel.close()

    def run(self):
        """Run the example consumer by connecting to RabbitMQ and then
        starting the IOLoop to block and allow the SelectConnection to operate.

        """
        self._connection = self.connect()
        self._connection.ioloop.start()

    def stop(self):
        """Cleanly shutdown the connection to RabbitMQ by stopping the consumer
        with RabbitMQ. When RabbitMQ confirms the cancellation, on_cancel_ok
        will be invoked by pika, which will then closing the channel and
        connection. The IOLoop is started again because this method is invoked
        when CTRL-C is pressed raising a KeyboardInterrupt exception. This
        exception stops the IOLoop which needs to be running for pika to
        communicate with RabbitMQ. All of the commands issued prior to starting
        the IOLoop will be buffered but not processed.

        """
        if not self._closing:
            self._closing = True
            logger.info("Stopping")
            if self._consuming:
                self.stop_consuming()
                self._connection.ioloop.start()
            else:
                self._connection.ioloop.stop()
            logger.info("Stopped")


def get_consumer(config) -> Consumer:
    """Sets up a new MQ consumer."""
    callback = ConsumerCallback(config)
    consumer_config = ConsumerConfig(**config.get("tofh.plugins.mq", {}))
    conn_args = get_connection_params(consumer_config)
    return Consumer(
        conn_args,
        callback,
        consumer_config.exchanges,
        consumer_config.queues,
        consumer_config.bindings,
        consumer_config.consume_queues,
        setup=Consumer.STEPS.DECLARE_EXCHANGE,
        consumer_tag=consumer_config.consumer_tag,
    )
