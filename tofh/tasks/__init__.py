import collections
import enum

import attr
import pkg_resources


@enum.unique
class TaskType(enum.Enum):
    stateless = "stateless"
    stateful = "stateful"


def get_entry_point(name, entry_point):
    """Create an EntryPoint object.

    :param str name: the entry point name
    :param str entry_point: an entry-point like import specification

    :return: The entry point
    :rtype: pkg_resources.EntryPoint
    """
    return pkg_resources.EntryPoint.parse(f"{name} = {entry_point}")


def convert_list(*wrap_types):
    """Make a function that casts to list.

    :param wrap_types:
        if the input value is one of these types, it is wrapped in a list, and
        not cast to list.
    """

    def make_list(value):
        if value is None:
            return []
        if isinstance(value, wrap_types):
            return [value]
        return list(value)

    return make_list


def convert_none(wrap_type):
    """Wrap a converter to allow ``None``.

    :param wrap_type:
        The type to use, if the value is not ``None``.
    """

    def convert(value):
        if value is None:
            return None
        return wrap_type(value)

    return convert


def convert_task(value):
    """ Convert value to a TaskInfo object, if needed. """
    if isinstance(value, (dict, collections.abc.Mapping)):
        task = TaskInfo.from_dict(value)
    else:
        task = value
    if not isinstance(task, TaskInfo):
        raise ValueError(f"Invalid Task {value!r}")
    return task


@attr.s()
class TaskInfo:
    """Configuration of a single task.

    >>> task = TaskInfo('add', 'operators:add')
    >>> task.name
    'add'
    >>> task.call
    'operators:add'
    """

    name = attr.ib(converter=str)
    call = attr.ib(converter=str)
    task_type = attr.ib(default=TaskType.stateless, converter=TaskType)

    timeout_soft = attr.ib(default=None, converter=convert_none(int))
    timeout_hard = attr.ib(default=120, converter=convert_none(int))
    retry_max = attr.ib(default=2, converter=convert_none(int))
    retry_delay = attr.ib(default=180, converter=int)
    priority = attr.ib(default=None, converter=convert_none(int))

    triggers = attr.ib(default=None, converter=convert_list(dict))

    schedule_type = attr.ib(default=None, converter=str)
    schedule_frequency = attr.ib(default=None, converter=str)

    description = attr.ib(default=None, converter=str)

    @call.validator
    def _validate_call(self, attribute, value):
        if not pkg_resources.EntryPoint.pattern.match(f"foo={value}"):
            raise ValueError(f"Invalid call {value!r}")

    @triggers.validator
    def _validate_triggers(self, attribute, value):
        if not isinstance(value, list):
            raise ValueError(f"Invalid triggers {value!r}")
        for i, item in enumerate(value):
            if not isinstance(item, dict):
                raise ValueError(f"Invalid trigger (#{i}) {item!r}")

    @property
    def entry_point(self):
        """ entry point for this object. """
        return get_entry_point(self.name, self.call)

    def get_triggers(self, trigger_type):
        return [t for t in self.triggers if trigger_type == t.get("type")]

    def to_dict(self):
        return attr.asdict(self)

    @classmethod
    def from_dict(cls, d):
        return cls(**d)


# class Trigger(abc.ABC):

#     def __init__(self, args):
#         self.type = args.pop('type')
#         self.args = args

#     @abc.abstractmethod
#     def transform(self, event):
#         """ Transform an object into arguments and keyword arguments. """
#         pass

#     @abc.abstractmethod
#     def match(self, event):
#         """ Transform an object into a list of tasks and stuff. """
#         pass


class TaskCollection(collections.abc.Mapping):
    """A read-only dict-like collection of TaskInfo objects.

    A simple mapping implementation that asserts that each value is of type
    ``TaskInfo``.

    The mapping is initialized with a sequence of ``TaskInfo`` objects.
    """

    def __init__(self, initial=None):
        self.tasks = collections.OrderedDict()

        for task in initial or ():
            task_info = convert_task(task)
            self.tasks[task_info.name] = task_info

    def __len__(self):
        return len(self.tasks)

    def __getitem__(self, name):
        return self.tasks[name]

    def __iter__(self):
        return iter(self.tasks)

    def __repr__(self):
        return "<{cls.__name__} tasks={num}>".format(cls=type(self), num=len(self))


class TaskDescriptor:
    """A TaskCollection descriptor.

    This descriptor asserts that the owner object always have an associated
    TaskCollection object.  If the descriptor is deleted, a new empty
    collection is created in its place.
    """

    @property
    def attr(self):
        return "_{cls.__name__}__{id:02x}".format(cls=type(self), id=id(self))

    def __get__(self, obj, cls=None):
        if obj is None:
            return self

        tasks = getattr(obj, self.attr, None)
        if tasks is None:
            tasks = TaskCollection()
            setattr(obj, self.attr, tasks)
        return tasks

    def __set__(self, obj, value):
        if not isinstance(value, TaskCollection):
            value = TaskCollection(value)
        setattr(obj, self.attr, value)

    def __delete__(self, obj):
        new = TaskCollection()
        setattr(obj, self.attr, new)
