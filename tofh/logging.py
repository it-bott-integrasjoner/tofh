"""Configuration and initialization of logging and Sentry hooks"""
import logging.config
import pythonjsonlogger.jsonlogger

import sentry_sdk
from sentry_sdk.integrations.celery import CeleryIntegration
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.logging import LoggingIntegration

import tofh.config


logger = logging.getLogger(__name__)


class SyslogJsonFormatter(pythonjsonlogger.jsonlogger.JsonFormatter):
    """
    Wrapper around JsonFormatter.

    Something in JsonFormatter does not play nice with SyslogHandler
    This results in errors in the Json output.

    Writing the process name first "fixes" the problem.
    """
    def format(self, record):
        return '{0} {1}'.format(record.processName, super().format(record))


def configure_logging(config=None):
    """
    Configures logging.

    :param config: The app config
    :type config: tofh.config.AppConfig
    """
    config = config or tofh.config.get_config()
    logging.config.dictConfig(config.logging)


def configure_sentry(config=None):
    """
    Configures and intitializes Sentry.

    :param config: The app config
    :type config: tofh.config.AppConfig
    """
    config = config or tofh.config.get_config()

    if not config.sentry.get('enable', False):
        return
    dsn = config.sentry.get('dsn')
    if not dsn:
        raise Exception('Missing Sentry DSN')

    name_to_integration = {
        'logging': LoggingIntegration,
        'celery': CeleryIntegration,
        'flask': FlaskIntegration,
    }
    integrations = list()
    for name, kwargs in config.sentry.get('integrations', {}).items():
        if not kwargs.pop('enable', False):
            continue
        klass = name_to_integration.get(name)
        integrations.append(klass(**kwargs))

    environment = config.sentry.get('environment')
    release = config.sentry.get('release')

    sentry_sdk.init(
        dsn=dsn,
        integrations=integrations,
        default_integrations=True,
        environment=environment,
        release=release
    )


def configure(config=None):
    """
    Configures and intitializes logging and Sentry.

    :param config: The app config
    :type config: tofh.config.AppConfig
    """
    configure_logging(config)
    configure_sentry(config)
