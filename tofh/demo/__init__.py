""" Simple examples that can be used as demos or debug tasks. """
import logging
import random
import time

from tofh.tofh_demo import get_config

logger = logging.getLogger(__name__)

def extract_body_transform(event):
    return (event.body, {})

def use_a_config_variable(*args, **kwargs):
    # Service name is specified, so we can refer to the config by an
    # environment variable named DEMO_CONFIG
    logger.info("I fetched a variable from the configuration: %s",
                get_config(service_name='demo').get('my_var'))


def stateful_task(self, *args, **kwargs):
    if getattr(self, 'var', None):
        logger.info('self.var is %d', self.var)
    else:
        logger.info('self.var is unset')
    self.var = random.randint(0, 100)
    logger.info('self.var has been set to %d', self.var)


def failing_stateful_task(self, *args, **kwargs):
    if getattr(self, 'var', None):
        logger.info('self.var is %d', self.var)
    else:
        logger.info('self.var is unset')
    self.var = random.randint(0, 100)
    logger.info('self.var has been set to %d', self.var)
    logger.info('Provoking TypeError')
    'a' + 1

def echo(*args, **kwargs):
    """ Return all arguments

    :return tuple: ``(args, kwargs)``
    """
    logger.debug('echo args=%r, kwargs=%r', args, kwargs)
    return args, kwargs


def fail():
    """ Cause an exception.

    :raises RuntimeError: Always raises an exception.
    """
    logger.debug('fail')
    fail_conditional(True)


def fail_random():
    """ Randomly cause an exception.

    :raises RuntimeError: Raised randomly (about half of the calls).
    """
    logger.debug('fail_randomly')
    do_fail = random.random((True, False))
    fail_conditional(do_fail)


def fail_conditional(do_fail):
    """ Cause an exception if given a boolean true argument.

    :type do_fail: int, bool

    :raises RuntimeError: Raised if ``do_fail`` is ``True``.
    """
    logger.debug('fail_conditionally do_fail=%r', do_fail)
    if not isinstance(do_fail, (int, bool)):
        try:
            do_fail = int(do_fail)
        except (TypeError, ValueError):
            do_fail = bool(do_fail)
    if do_fail:
        raise RuntimeError("requested to fail (%r)".format(do_fail))


def sleep(n):
    """ Sleep for ``n`` seconds.

    :param float n: seconds to sleep
    :return float: seconds slept
    """
    logger.debug('sleep n=%r', n)
    seconds = float(n)
    start = time.time()
    logger.info('sleeping for %.02fs', seconds)
    time.sleep(seconds)
    return time.time() - start


def sleep_5():
    """ Sleep for 5 seconds. """
    logger.debug('sleep_5')
    return sleep(5)


def sleep_random(seconds_min=1, seconds_max=5):
    """ Sleep for a random amount of time. """
    logger.debug('sleep_random seconds_min=%r, seconds_max=%r',
                 seconds_min, seconds_max)
    seconds = random.randint(int(seconds_min), int(seconds_max))
    return sleep(seconds)
