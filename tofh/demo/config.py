import os
import sys
import functools

from tofh.config import AppConfig

SERVICE_NAME = __name__.split('.')[0]

FILES_ETC = os.path.join('' if sys.prefix == '/usr' else sys.prefix, 'etc')

CONFIG_NAME = 'config.yaml'

def get_config_lookup_order(service_name=SERVICE_NAME):
    return [
        os.getcwd(),
        os.path.expanduser(f'~/{SERVICE_NAME}'),
        os.path.join(FILES_ETC, f'{SERVICE_NAME}'),
    ]

def get_config_environ(service_name=SERVICE_NAME):
    return f'{SERVICE_NAME.upper()}_CONFIG'


class MSConfig(AppConfig):
    def __init__(self, service_name=SERVICE_NAME, conf_dict=None):
        super().__init__(conf_dict=conf_dict)
        setattr(self, service_name, self.extra.pop(service_name, {}))

    def get(self, name, default=None):
        if name in getattr(self, service_name):
            return getattr(self, service_name)[name]
        return default

def iter_config_locations(filename=CONFIG_NAME, lookup_order=None, service_name=SERVICE_NAME):
    CONFIG_ENVIRON = get_config_environ(service_name)
    if CONFIG_ENVIRON in os.environ:
        yield os.path.abspath(os.environ[CONFIG_ENVIRON])
    for path in (lookup_order or get_config_lookup_order(service_name)):
        yield os.path.abspath(os.path.join(path, filename))


@functools.lru_cache()
def get_config(filename: str = None, service_name: str = SERVICE_NAME):
    if filename is None:
        for filename in filter(os.path.exists, iter_config_locations(service_name=service_name)):
            break
    if filename:
        return MSConfig.from_file(filename)
    return MSConfig()

