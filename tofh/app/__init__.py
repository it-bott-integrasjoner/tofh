"""celery app and task."""
import inspect
import logging

import celery
from celery.utils.time import get_exponential_backoff_interval
# import celery.signals

import tofh.config
import tofh.tasks
import tofh.logging

logger = logging.getLogger(__name__)


def task_info_to_params(task_info):
    return {
        'name': task_info.name,
        'max_retries': task_info.retry_max,
        'default_retry_delay': task_info.retry_delay,
        'time_limit': task_info.timeout_hard,
        'soft_time_limit': task_info.timeout_soft,
    }


class TaskInfoWrapper(celery.Task):
    """
    Task implementation for delayed entry point resolve.

    An alternative celery task implementation class that delays loading the
    task entry point until runtime.

    :type app: celery.Celery
    :type tasks: list
    :param tasks:
        A list of :py:class:`tofh.tasks.TaskInfo` objects to load.
    """

    def __init__(self, task_info):
        self.__dict__.update(task_info_to_params(task_info))
        self._entry_point = task_info.entry_point

    def _get_retry_countdown(self, **options):
        # Should always be 0, as we currently don't allow backoff or jitter
        # settings in task_info.
        backoff = int(getattr(self, 'retry_backoff', False))
        backoff_opts = {
            'factor': backoff,
            'retries': self.request.retries,
            'maximum': int(getattr(self, 'retry_backoff_max', 600)),
            'full_jitter': getattr(self, 'retry_jitter', True),
        }
        if backoff:
            logger.debug('retry opts=%r', backoff_opts)
            countdown = get_exponential_backoff_interval(**backoff_opts)
        else:
            countdown = None
        logger.debug('retry countdown=%r', countdown)
        return countdown

    def run(self, *args, **kwargs):
        """
        Run a task_info task, and handle errors.
        """
        func = self._entry_point.resolve()
        validate_args(func, args, kwargs)

        try:
            return func(*args, **kwargs)
        except Exception as e:
            retry_opts = {
                'countdown': self._get_retry_countdown(),
            }
            logger.error("Task %r failed, retry with opts=%r",
                         self.request.id, retry_opts,
                         exc_info=True)
            raise self.retry(exc=e, **retry_opts)


def load_stateful_tasks(app, tasks):
    """
    Load tasks from task list as stateful celery tasks.

    Each task entry point is loaded immediately, and wrapped using
    :py:func:`celery.Celery.task`, which the entry point is bound to.

    See :py:func:`tofh.demo.stateful_task` for an example.

    :type app: celery.Celery
    :type tasks: list
    :param tasks:
        A list of :py:class:`tofh.tasks.TaskInfo` objects to load.
    """
    for task in tasks:
        if task.task_type == tofh.tasks.TaskType.stateful:
            opts = task_info_to_params(task)
            opts['autoretry_for'] = (Exception, )
            app.task(bind=True, **opts)(task.entry_point.resolve())


def load_lazy_tasks(app, tasks):
    """
    Load tasks from task list as custom stateless celery tasks.

    Each task is wrapped in a :py:class:`TaskInfoWrapper` celery task, and
    added to the celery app using :py:func:`celery.Celery.register_task`.

    See :py:func:`tofh.demo.echo` for an example.

    :type app: celery.Celery
    :type tasks: list
    :param tasks:
        A list of :py:class:`tofh.tasks.TaskInfo` objects to load.
    """
    for task in tasks:
        if task.task_type == tofh.tasks.TaskType.stateless:
            app.register_task(TaskInfoWrapper(task))


def autoload_app(config=None):
    """
    Instantiate a celery app with custom TaskInfo tasks.

    :param config: The app config
    :type config: tofh.config.AppConfig
    """
    # Configure celery worker app
    config = config or tofh.config.get_config()

    app = celery.Celery(__name__)
    app.config_from_object(config.celery)

    tofh.logging.configure(config)

    load_stateful_tasks(app, config.tasks.values())
    load_lazy_tasks(app, config.tasks.values())

    @celery.signals.setup_logging.connect(weak=False)
    def _setup_logging(*args, **kwargs):
        # Sentry is already listening to relevant signals
        tofh.logging.configure_logging(config)
    return app


def validate_args(func, args, kwargs):
    """
    Verify arguments with function signature.

    :param callable func: any callable
    :param tuple args: positional arguments
    :param dict kwargs: keyword arguemnts

    :raises TypeError:
        iff there is a mismatch between arguments and function signature.
    """
    s = inspect.signature(func)
    s.bind(*args, **kwargs)
