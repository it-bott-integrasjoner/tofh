""" an app object for the celery workers. """
from . import autoload_app

app = autoload_app()
