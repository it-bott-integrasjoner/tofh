#!/usr/bin/env python3

"""
tofh configuration
==================

Format
------

    {
        'celery': { … }
        'flask': { … },
        'logging': { … },
        'tasks': [ … ],
        '…': …,
    }

celery
    ``config['celery']`` should be a dictionary where the keys and values are
    valid configuration options for celery.

    The actual dict is simply turned into a class (``type('celeryconfig',
    (object, ), config['celery'])``) and fed to
    ``Celery.config_from_object()``.

flask
    ``config['flask']`` should be a dictionary where the keys and values are
    valid configuration options for flask and the ``tofh.api`` flask app.

    The actual dict is simply fed to ``Flask.config.from_mapping()``.

logging
    ``config['logging']`` should be a dictionary where the keys and values are
    valid configuration options for the logging module.

    The actual dict is simply fed into ``logging.config.dictConfig()``.

tasks
    ``config['tasks']`` is a list of tasks. Each task is a dictionary with
    enough values to populate ``tofh.tasks.config.TaskInfo``.

<…>
    Any additional key is treated as plugin configuration, and can be fetched
    by 3rd party modules.


Location
--------

The configuration file is fetched from the first avaliable location:

1. A filename in os.environ['TOFH_CONFIG']
2. A config.yml in the current working directory.
3. A config.yml in ``~/.tofh/``
4. A config.yml in ``/etc/tofh/`` (or ``<sys.prefix>/etc/tofh/``)
"""

import os
import sys

import yaml

import tofh.tasks


FILES_ETC = os.path.join('' if sys.prefix == '/usr' else sys.prefix, 'etc')

CONFIG_NAME = 'config.yml'
CONFIG_LOOKUP_ORDER = [
    os.getcwd(),
    os.path.expanduser('~/tofh'),
    os.path.join(FILES_ETC, 'tofh'),
]
CONFIG_ENVIRON = 'TOFH_CONFIG'

DEFAULT_CONFIG_FLASK = {
    'DEBUG': True,
}


DEFAULT_CONFIG_LOGGING = {
    'disable_existing_loggers': False,
    'version': 1,
    'root': {
        'level': 'DEBUG',
        'handlers': ['debug'],
    },
    'loggers': {
        'pika': {
            'level': 'INFO',
        },
        'flask': {},
        'flask.app': {},
        'werkzeug': {},
    },
    'handlers': {
        'debug': {
            'class': 'logging.StreamHandler',
            'level': 'NOTSET',
            'formatter': 'simple',
            'stream': 'ext://sys.stderr',
        }
    },
    'filters': {},
    'formatters': {
        'simple': {
            'format': '%(levelname)s - %(name)s - %(message)s',
        }
    },
}

DEFAULT_CONFIG_SENTRY = {
    'enable': False,
    'dsn': None,
    'integrations': {},
}

CELERY_IMPORTS=("tofh.plugins.scheduler.__main__",)


class CeleryConfig(dict):
    def __init__(self, conf_dict=None):
        super().__init__(conf_dict if conf_dict is not None else {})
        self.__dict__ = self


class AppConfig:
    """tofh config."""

    tasks = tofh.tasks.TaskDescriptor()

    def __init__(self, conf_dict=None):
        conf_dict = dict(conf_dict or {})
        self.celery = CeleryConfig(conf_dict.pop('celery', {}))
        self.flask = conf_dict.pop('flask', DEFAULT_CONFIG_FLASK)
        self.logging = conf_dict.pop('logging', DEFAULT_CONFIG_LOGGING)
        self.sentry = conf_dict.pop('sentry', DEFAULT_CONFIG_SENTRY)
        self.tasks = conf_dict.pop('tasks', [])
        self.extra = conf_dict

    def get(self, name, default=None):
        return self.extra.get(name, default)

    def to_dict(self):
        rv = self.extra.copy()
        rv.update({
            "celery": self.celery,
            "flask": self.flask.copy(),
            "logging": self.logging.copy(),
            "sentry": self.sentry.copy(),
            "tasks": [task.to_dict() for task in self.tasks.values()],
        })
        return rv

    def __repr__(self):
        return '<{cls.__name__}>'.format(cls=type(self))

    @classmethod
    def from_yaml(cls, yamlstr):
        return cls(yaml.load(yamlstr, Loader=yaml.FullLoader))

    @classmethod
    def from_file(cls, filename):
        with open(filename, 'r') as f:
            return cls.from_yaml(f.read())


def iter_config_locations(filename=CONFIG_NAME, lookup_order=None):
    if CONFIG_ENVIRON in os.environ:
        yield os.path.abspath(os.environ[CONFIG_ENVIRON])
    for path in (lookup_order or CONFIG_LOOKUP_ORDER):
        yield os.path.abspath(os.path.join(path, filename))


def get_config(filename=None):
    if filename is None:
        for filename in filter(os.path.exists, iter_config_locations()):
            break

    if filename:
        return AppConfig.from_file(filename)
    return AppConfig()


# python -m tofh.config

def main(inargs=None):
    """ Print supported file formats. """
    import argparse
    import pprint

    parser = argparse.ArgumentParser()
    parser.add_argument(
        'filename',
        nargs='?',
        help="Parse file and pretty-print result")

    args = parser.parse_args(inargs)

    def pretty(data, indent='  '):
        d = pprint.pformat(data)
        print(indent + indent.join(d.splitlines(True)), '\n')

    def list_configs():
        print("Lookup order:")
        for f in iter_config_locations():
            exists = 'yes' if os.path.exists(f) else 'no'
            print(f'  {f} (exists: {exists})')

    def show_config(filename):
        print('Filename:', repr(filename), '\n')
        config = get_config(filename)

        print('Celery')
        pretty(dict((k, getattr(config.celery, k))
                    for k in config.celery.__dict__
                    if not k.startswith('_')))

        print('Flask')
        pretty(config.flask)

        print('Logging')
        pretty(config.logging)

        print('Tasks:', repr(config.tasks))
        for task in config.tasks.values():
            print('  -', repr(task))
        print()

    if args.filename:
        show_config(args.filename)
    else:
        list_configs()


if __name__ == '__main__':
    main()
